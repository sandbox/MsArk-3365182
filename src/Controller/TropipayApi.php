<?php

namespace Drupal\tropipay_api\Controller;

use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Url;
use Drupal\Core\Render\Markup;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\DrupalKernel;
use Drupal\user\Entity\User;

class TropipayApi {

  /**
   * Retrive token from user.
   */
  public function getUserCode() {
    // Retrieves the configuration settings.
    $config = \Drupal::config('tropipay_api.settings');
    // Variables from the configuration.
    $url = $config->get('url');
    $clientId = $config->get('client_id');
    $clientSecret = $config->get('client_secret');
    $state = $config->get('state');
    // URL parámeters.
    $paramState = \Drupal::request()->query->get('state');
    $paramCode = \Drupal::request()->query->get('code');

    // Check state.
    if ($state == $paramState) {
      // Get access token from tropipay user.
      $access_token = $this->getAccessToken($url, $clientId, $clientSecret, $paramCode);
      // Get profile from tropipay user.
      $create_user_profile = $this->getUserProfile($access_token);

      // Variables to html page.
      $site_config = \Drupal::config('system.site');
      $variables['site_name'] = $site_config->get('name');
      $variables['logopath'] = file_url_transform_relative(file_create_url(theme_get_setting('logo.url')));
      // Render HTML.
      $html = '<div style="text-align: center;">';
      $html .= '<img src="' . $variables['logopath'] . '" style="width: 200px" />';
      $html .= '<h1>' . $variables['site_name'] . '</h1>';
      $html .= '<div style="text-align: center; margin: 5rem auto; width: 420px;">';
      $html .= '<p style="text-transform: uppercase; font-size: 24px;">' . $create_user_profile . '</p>';
      $html .= '</div>';
      $html .= '<a href="' . Url::fromRoute('<front>')->toString() . '" style="border: 1px solid rgb(13, 110, 253); background-color: rgb(13, 110, 253); color: white;
              padding: 10px 20px; border-radius: 30px; cursor: pointer; display: inline-block; font-size: 20px; text-decoration: none;">Volver a la página principal</a>';
      $html .= '</div>';
    } else {
      $error = t('An error has occurred');
      // Render HTML.
      $html = '<div style="text-align: center;">';
      $html .= '<img src="' . $variables['logopath'] . '" style="width: 200px" />';
      $html .= '<h1>' . $variables['site_name'] . '</h1>';
      $html .= '<div style="text-align: center; margin: 5rem auto; width: 420px;">';
      $html .= '<p style="text-transform: uppercase; font-size: 24px; color: red;">' . $error . '</p>';
      $html .= '</div>';
      $html .= '<a href="' . Url::fromRoute('<front>')->toString() . '" style="border: 1px solid rgb(13, 110, 253); background-color: rgb(13, 110, 253); color: white;
              padding: 10px 20px; border-radius: 30px; cursor: pointer; display: inline-block; font-size: 20px; text-decoration: none;">Volver a la página principal</a>';
      $html .= '</div>';
    }

    return new Response($html);
  }

  /**
   * Return access token from tropipay user.
   */
  public function getAccessToken($url, $clientId, $clientSecret, $paramCode) {
    $request = \Drupal::request();
    $http_client = \Drupal::httpClient();

    // Endpoint formated.
    $endpoint = $url . '/api/v2/access/token';
    // Body to POST.
    $body = array(
      'grant_type' => 'authorization_code',
      'code' => $paramCode,
      'client_id' => $clientId ,
      'client_secret' => $clientSecret,
      'scope' => 'ALLOW_GET_PROFILE_DATA ALLOW_GET_BALANCE'
    );

    try {
      // Request POST.
      $response = $http_client->request('POST', $endpoint, [
        'json' => $body,
        'headers' => ['Content-Type' => 'application/json'],
      ]);

      if ($response->getStatusCode() == 200) {
        // Return data.
        $data = json_decode($response->getBody(), TRUE);

        // Check access token.
        if (isset($data['access_token'])) {
          $access_token = $data['access_token'];

          return $access_token;
        }
      } else {
        // Error into the request.
        return 'An error occurred in the registration. please try again.';
      }
    } catch (\Exception $e) {
      // Error into the request.
      return 'An error occurred while making the request. please try again.';
    }
  }

  /**
   * Return profile from tropipay user.
   */
  public function getUserProfile($access_token) {
    // Retrieves the configuration settings.
    $config = \Drupal::config('tropipay_api.settings');
    // Variables to format url.
    $url = $config->get('url');
    // Format to the url format.
    $endpoint = $config->get('url') . '/api/users/profile';

    try {
      $http_client = \Drupal::httpClient();
      $response = $http_client->request('GET', $endpoint, [
        'headers' => [
          'Authorization' => 'Bearer ' . $access_token,
          'Accept' => 'application/json',
        ],
      ]);

      if ($response->getStatusCode() == 200) {
        $profile_data = json_decode($response->getBody(), TRUE);

        // Check user email exits.
        $check_user_mail = \Drupal::entityQuery('user')
          ->condition('mail', $profile_data['email'])
          ->execute();

        if (!empty($check_user_mail)) {
          // Get user uid.
          $uid = array_shift(array_values($check_user_mail));
          $user = \Drupal\user\Entity\User::load($uid);
          user_login_finalize($user);
          $response = new \Symfony\Component\HttpFoundation\RedirectResponse('/');
          $request  = \Drupal::request();
          $request->getSession()->save();
          $response->prepare($request);
          \Drupal::service('kernel')->terminate($request, $response);
          $username = $user->getAccountName();
          \Drupal::messenger()->addMessage(t('You are logged with username ' . $username ));
          return $response->send();
        }

        // Check if user is company or physical person.
        if (!empty($profile_data['clientTypeId']) && $profile_data['clientTypeId'] == 1) {
          return t('The account could not be created because it is not a company account.');
        } elseif (!empty($profile_data['clientTypeId']) && $profile_data['clientTypeId'] == 2) {
          // Manage user profile data according to your needs.
          $username = $profile_data['name'];
          $email = $profile_data['email'];
          $user_surname = (!empty($profile_data['surname'])) ? $profile_data['surname'] : null;
          $user_company = (!empty($profile_data['company'])) ? $profile_data['company'] : null;
          $user_business = (!empty($profile_data['business'])) ? $profile_data['business'] : null;
          $user_address = (!empty($profile_data['address'])) ? $profile_data['address'] : null;
          $user_province = (!empty($profile_data['province'])) ? $profile_data['province'] : null;
          $user_sex = (!empty($profile_data['sex'])) ? $profile_data['sex'] : null;
          $user_phone = (!empty($profile_data['phone'])) ? $profile_data['phone'] : null;
          $user_tropipay_verified = (!empty($profile_data['verified'])) ? $profile_data['verified'] : null;

          // Create a new user object in Drupal
          $user = \Drupal\user\Entity\User::create();
          $user->setPassword(user_password());
          $user->enforceIsNew();
          $user->setEmail($email);
          $user->setUsername($username);

          // We check if there is data.
          if ($user_surname !== null) {
            $user->set('field_user_surname', $user_surname);
          }
          if ($user_company !== null) {
            $user->set('field_user_company', $user_company);
          }
          if ($user_address !== null) {
            $user->set('field_user_address', $user_address);
          }
          if ($user_province !== null) {
            $user->set('field_user_province', $user_province);
          }
          if ($user_sex !== null) {
            $user->set('field_user_sex', $user_sex);
          }
          if ($user_phone !== null) {
            $user->set('field_user_phone', $user_phone);
          }
          if ($user_tropipay_verified !== null) {
            $user->set('field_user_tropipay_verified', $user_tropipay_verified);
          }
          $user->block();
          $user->save();

          return t('User created, an email will be sent to you.');
        }
      } else {
        // Ocurrió un error en la solicitud.
        \Drupal::messenger()->addError('An error occurred while getting the users profile. please try again.');
        }
    } catch (\Exception $e) {
      // Error al realizar la solicitud.
      \Drupal::messenger()->addError('An error occurred while making the request. please try again.');
    }
  }
}
