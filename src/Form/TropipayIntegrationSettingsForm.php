<?php

namespace Drupal\tropipay_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Tropipay Integration settings form.
 */
class TropipayIntegrationSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tropipay_api_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['tropipay_api.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('tropipay_api.settings');

    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API URL'),
      '#default_value' => $config->get('url'),
      '#description' => $this->t('Enter the URL for the Tropipay API.'),
      '#name' => 'url',
    ];

    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('client_id'),
      '#description' => $this->t('Enter the Client ID for the Tropipay API.'),
    ];

    // TODO: Encriptar
    $form['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#default_value' => $config->get('client_secret'),
      '#description' => $this->t('Enter the Client Secret for the Tropipay API.'),
    ];

    $form['state'] = [
      '#type' => 'textfield',
      '#title' => $this->t('State variable'),
      '#default_value' => $config->get('state'),
      '#description' => $this->t('Enter the State to verify the non-modification of the request.'),
    ];

    $form['scope'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Tropipay Integration Scope'),
    ];

    $form['scope']['allow_get_profile_data'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow Get Profile Data'),
      '#default_value' => $config->get('allow_get_profile_data'),
      '#description' => $this->t('Enable this option to allow getting profile data from Tropipay.'),
    ];

    $form['scope']['allow_get_balance'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow Get Balance'),
      '#default_value' => $config->get('allow_get_balance'),
      '#description' => $this->t('Enable this option to allow getting balance from Tropipay.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('tropipay_api.settings')
      ->set('url', $form_state->getValue('url'))
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('state', $form_state->getValue('state'))
      ->set('allow_get_profile_data', $form_state->getValue('allow_get_profile_data'))
      ->set('allow_get_balance', $form_state->getValue('allow_get_balance'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
